DOCKER_IMAGE = quay.ac.uda.io/jinchenbravo/dora
DOCKER_INSTANCE = dora_dora_1
npminstall :
	npm i
npmbuild : npminstall
	npm run build
dockerbuild :
	docker-compose build
build : npmbuild dockerbuild
clean : stop
	docker-compose rm -f
up :
	docker-compose up -d
stop : 
	docker stop $(DOCKER_INSTANCE)
push : build
	docker push $(DOCKER_IMAGE)
pull :
	docker pull $(DOCKER_IMAGE)
prune :
	docker system prune -f
list :
	docker images
rmi : prune
	docker rmi $(DOCKER_IMAGE)

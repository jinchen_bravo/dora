import 'roboto-fontface/css/roboto/roboto-fontface.css';
import 'angular-material/angular-material.min.css';
import 'material-design-icons/iconfont/material-icons.css';
import 'font-awesome/css/font-awesome.min.css';
import angular from 'angular';
import html from './app.html';
import './pages/server';
import './pages/workflow';
import './pages/dbandtbl';
import 'angular-material';
import './models';
import './filters';
let app = () => {
  return {
    template: html,
    controller: AppCtrl,
    controllerAs: 'app'
  }
};

class AppCtrl {
  constructor(mdSidenav, baseurl) {
    this.baseurl = baseurl;
    this.mdSidenav = mdSidenav;
    this.menus = [{ label: 'Databases/Tables', icon: 'storage' }, { label: 'Servers', icon: 'dvr' }, { label: 'Workflows', icon: 'device_hub' }];
  }
  openSidenav() {
    this.mdSidenav('left').open();
  }
  closeSidenav() {
    this.mdSidenav('left').close();
  }
  switch(menu) {
    this.servers = false;
    this.workflows = false;
    this.databases = false;
    this.menu = menu;
    if (menu.label === 'Servers') {
      this.servers = true;
    }
    if (menu.label === 'Workflows') {
      this.workflows = true;
    }
    if (menu.label === 'Databases/Tables') {
      this.databases = true;
    }
    this.closeSidenav();
  }
}
AppCtrl.$inject = ['$mdSidenav', 'baseurl'];
let config = ($locationProvider, $mdThemingProvider, baseurlProvider, $windowProvider) => {
  $locationProvider.hashPrefix('!');
  // $routeProvider.otherwise({
  //   redirectTo: '/serverview'
  // });
  $mdThemingProvider.theme('default')
    .primaryPalette('blue')
    .accentPalette('blue-grey');
  let x = $windowProvider.$get().location.hash;
  baseurlProvider.setUrl(x)
};

const MODULE_NAME = 'app';
// let apprun = (baseurl) => {
//   console.log(baseurl);
// }
let baseurl = () => {
  let url = undefined;
  return {
    setUrl: function (e) {
      if (!e.split('/')[1]) {
        url = 'http://kiba:8090/emrest/datadiscovery'
      } else if (e === '#!/dev') {
        url = 'http://kiba:8090/emrest/datadiscovery'
      } else if (e === '#!/qa') {
        url = 'http://sdt18914.labs.teradata.com:8090/emrest/datadiscovery';
      } else {
        url = `http://${e.split('/')[1]}:8090/emrest/datadiscovery`;
      }
    },
    $get: function () {
      return url
    }
  }
}
angular.module(MODULE_NAME, ['app.filters', 'app.models', 'app.pages.servers', 'app.pages.workflows', 'app.pages.dbandtbl', 'ngMaterial'])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl)
  .provider('baseurl', baseurl)
  .config(config)
  // .run(apprun)
  // .constant('RESTURL', `${baseurl}/emrest/datadiscovery`)
  .constant('basicReadOnly', `Basic ${window.btoa('emrestuser:emrest@user')}`)
  .constant('basicReadWrite', `Basic ${window.btoa('emrestsuper:emrest@super')}`);

export default MODULE_NAME;

import WorkflowDirective from './workflow.directive';
import '../../services';
export default angular
  .module('app.pages.workflows', ['zApp.service'])
  .component('workflowList', WorkflowDirective);

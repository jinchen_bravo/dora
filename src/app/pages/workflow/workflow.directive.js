import html from './workflow-list.html';
import { WorkflowController } from './workflow.controller';
export default {
  template: html,
  controller: WorkflowController
}

import throbberhtml from '../common/throbber.html'
import sswfhtml from './sswf.tmpl.html'
import objswfhtml from './objswf.tmpl.html'
import workflowdetailhtml from './workflow-detail.html'
import restarthtml from './restart.html'
import schedulehtml from './schedule.html'

class WorkflowController {
  constructor(t, mdDialog, s, wffactory, scope) {
    this.WorkflowService = s;
    this.workflows = [];
    this.mdDialog = mdDialog;
    this.mdToast = t;
    this.workflowFactory = wffactory;
    this.scope = scope;
  }
  $onInit() {
    this.workflows = this.WorkflowService.workflow.query();
  }
  openMenu($mdMenu, ev) {
    //originatorEv = ev;
    $mdMenu.open(ev);
  };
  _showthrobber() {
    this.mdDialog.show({
      template: throbberhtml,
      parent: angular.element(document.body),
      escapeToClose: false,
      clickOutsideToClose: false,
      fullscreen: false
    })
  }
  save_objswf(wf) {
    this._showthrobber();
    console.log(JSON.stringify(wf));
    let x = this.workflowFactory.object_seeding.create(`ngobjs-${wf.source_td_pid}-${wf.target_td_pid}`);
    x.srcdb = { name: wf.source_td_pid, credential: { username: 'dbc', password: 'dbc' } }
    x.tgtdb = { name: wf.target_td_pid, credential: { username: 'dbc', password: 'dbc' } }
    x.dm = { dm_permisson: true, server_name: wf.dm_host, credential: { username: 'dmuser', password: 'dmpass' } }
    x.cam = 'Administrator Default';
    console.log(JSON.stringify(x));
    this.WorkflowService.object_seeding.update(x, null,
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  save_sswf(wf) {
    this._showthrobber();
    //console.log(JSON.stringify(wf));
    let x = this.workflowFactory.system_seeding.create(`ngss-${wf.source_td_pid}-${wf.target_td_pid}`);
    x.srcdb = { name: wf.source_td_pid, credential: { username: 'dbc', password: 'dbc' } }
    x.tgtdb = { name: wf.target_td_pid, credential: { username: 'dbc', password: 'dbc' } }
    x.tgtos = { name: wf.target_td_pid, credential: { username: 'root', password: wf.target_os_user_password ? wf.target_os_user_password : 'TCAMPass123' } }
    x.tgtauthtype = wf.authType ? wf.authType : 'PASSWORD';
    x.cam = 'Administrator Default';
    x.sm = 'ARCMAIN';
    x.st = wf.seedingType ? wf.seedingType : 'F';
    x.encrypt = wf.encryptFlag ? wf.encryptFlag : true;
    //x.addtional_not_necessary_propperty = 'xyz'
    //console.log(JSON.stringify(x));
    this.WorkflowService.system_seeding.update(x, null,
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  fetch(wf) {
    this._showthrobber();
    this.WorkflowService.workflow.get({ name: wf.workflow_name }, null,
      (resp, fn, code, text) => {
        //console.log(JSON.stringify(resp));
        this.workflow = resp;
        this.mdDialog.show({
          template: workflowdetailhtml,
          scope: this.scope,
          preserveScope: true,
          clickOutsideToClose: false,
          escapeToClose: false,
        })
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      });
  }
  add() {
    this.mdDialog.show({
      template: sswfhtml,
      scope: this.scope,
      preserveScope: true,
      clickOutsideToClose: false,
      escapeToClose: false,
    })
  }
  add_objswf() {
    this.mdDialog.show({
      template: objswfhtml,
      scope: this.scope,
      preserveScope: true,
      clickOutsideToClose: false,
      escapeToClose: false,
    })
  }
  start(wf, ev) {
    this._showthrobber();
    this.WorkflowService.workflow_execution.execution({ name: wf.workflow_name }, { execution_type: 'START' },
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  show_restart(wf, ev) {
    this.workflow = wf;
    this.mdDialog.show({
      template: restarthtml,
      scope: this.scope,
      preserveScope: true,
      clickOutsideToClose: false,
      escapeToClose: false,
    })
  }
  restart(restart) {
    this._showthrobber();
    this.WorkflowService.workflow_execution.execution({
      name: this.workflow.workflow_name
    },
      {
        execution_type: 'RESTART',
        failed_node_name: restart.failNode
      },
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  rollback(wf, ev) {
    this._showthrobber();
    this.WorkflowService.workflow_execution.execution({ name: wf.workflow_name }, { execution_type: 'ROLLBACK' },
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  pause(wf, ev) {
    this._showthrobber();
    this.WorkflowService.workflow_execution.execution({ name: wf.workflow_name }, { execution_type: 'PAUSE' },
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  resume(wf, ev) {
    this._showthrobber();
    this.WorkflowService.workflow_execution.execution({ name: wf.workflow_name }, { execution_type: 'RESUME' },
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  deploy(wf, ev, index) {
    this._showthrobber();
    this.WorkflowService.workflow_action.action({ name: wf.workflow_name }, { action_type: 'DEPLOY' },
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast
          .simple()
          .action('OK')
          .highlightAction(true)
          .hideDelay(0)
          //.position('right')
          //.parent(ev.target)
          .textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  undeploy(wf) {
    this._showthrobber();
    this.WorkflowService.workflow_action.action({ name: wf.workflow_name }, { action_type: 'UNDEPLOY' },
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  show_schedule(wf, ev) {
    this.workflow = wf;
    this.mdDialog.show({
      template: schedulehtml,
      scope: this.scope,
      preserveScope: true,
      clickOutsideToClose: false,
      escapeToClose: false,
    })
  }
  schedule(schedule) {
    this._showthrobber();
    this.WorkflowService.workflow_action.action({
      name: this.workflow.workflow_name
    }, {
        action_type: 'SCHEDULE',
        schedule_cron: schedule.scheduleCron,
        schedule_time_zone: schedule.scheduleTimeZone
      },
      (resp, fn, code, text) => {
        this.mdDialog.cancel();
        this.$onInit();
      },
      (err) => {
        this.mdDialog.cancel();
        this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
      })
  }
  remove(wf, ev) {
    let confirm = this.mdDialog.confirm()
      .title(`Would you like to delete workflow ${wf.workflow_name}?`)
      .targetEvent(ev)
      .ok('YES')
      .cancel('NO');

    this.mdDialog.show(confirm).then(() => {
      this._showthrobber();
      this.WorkflowService.workflow.remove({ name: wf.workflow_name }, null,
        (resp, fn, code, text) => {
          this.mdDialog.cancel();
          this.$onInit();
        },
        (err) => {
          this.mdDialog.cancel();
          this.mdToast.show(this.mdToast.simple().textContent(`Error Code: ${err.status} ${JSON.stringify(err.data)}`));
          // console.log(JSON.stringify(err.data));
          // console.log(JSON.stringify(err));
        })
    }, () => {
    });
  };
}
WorkflowController.$inject = ['$mdToast', '$mdDialog', 'WorkflowService', 'workflowFactory', '$scope'];
export { WorkflowController };

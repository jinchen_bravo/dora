import ServerDirective from './server.directive';
import '../../services';
import './servers.css';
export default angular
  .module('app.pages.servers', ['zApp.service'])
  .directive('serverList', ServerDirective);

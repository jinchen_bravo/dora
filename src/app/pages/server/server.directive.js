import html from './server-view.html';
import { ServerController as ctrl } from './server.controller';
let ServerDirective = () => {
  return {
    template: html,
    controller: ctrl,
    controllerAs: 'ctrl'
  }
}

export default ServerDirective;

import html from './server.tmpl.html'

class ServerController {
  constructor(mdDialog, s) {
    this.ServerService = s;
    this._refresh_list();
    this.name = "Server Name inside constructor";
    this.mdDialog = mdDialog;
  }
  _refresh_list() {
    this.servers = this.ServerService.query();
  }
  remove(s, ev) {
    let confirm = this.mdDialog.confirm()
      .title(`Would you like to delete server ${s.server_name}?`)
      .targetEvent(ev)
      .ok('YES')
      .cancel('NO');

    this.mdDialog.show(confirm).then(() => {
      this.ServerService.remove({ name: s.server_name }, s,
        (resp, fn, code, text) => {
          // console.log(JSON.stringify(resp));
          // console.log(code);
          // console.log(text);
          this.mdDialog.cancel();
          this._refresh_list();
        },
        (err) => {
          console.log(JSON.stringify(err.data));
          console.log(JSON.stringify(err));
        })
    }, () => {
    });
  };

  fetch(d, ev) {
    this.mdDialog.show({
      template: html,
      targetEvent: ev,
      clickOutsideToClose: true,
      controller: ($scope, $mdDialog) => {
        $scope.data = d;
        $scope.cancel = () => {
          $mdDialog.cancel();
          this._refresh_list();
        };
        $scope.save = (data) => {
          this.ServerService.update({ name: data.server_name }, data,
            (resp, fn, code, text) => {
              // console.log(JSON.stringify(resp));
              // console.log(code);
              // console.log(text);
              $scope.cancel();
            },
            (err) => {
              $scope.error = err;
              // console.log(JSON.stringify(err.data));
              // console.log(JSON.stringify(err));
            });
        };
      }
    })
  }
  submit(s) {
    console.log('submit form');
    this.ServerService.update({ name: s.server_name }, s, function (resp, fn, code, text) {
      console.log(JSON.stringify(resp));
      console.log(code);
      console.log(text);
    }, function (err) {
      console.log(JSON.stringify(err.data));
      console.log(JSON.stringify(err));
    });
  }
}
ServerController.$inject = ['$mdDialog', 'ServerService'];
export { ServerController };

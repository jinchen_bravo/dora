import html from './database-list.html';
import { DatabaseController as dbctrl } from './database.controller';
let DatabaseDirective = () => {
  return {
    template: html,
    controller: dbctrl,
    controllerAs: 'dbctrl'
  }
}

export default DatabaseDirective;

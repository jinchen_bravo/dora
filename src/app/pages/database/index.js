import DatabaseDirective from './database.directive';
import '../../services';
export default angular
  .module('app.pages.databases', ['zApp.service'])
  .directive('databaseList', DatabaseDirective);

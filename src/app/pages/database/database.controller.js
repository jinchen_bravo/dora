
class DatabaseController {
  constructor(scope, mdDialog, s) {
    this.scope = scope;
    this.DatabaseService = s;
    this._refresh_list();
    this.mdDialog = mdDialog;
    this.scope.$on('refresh:databaselist', (event) => {
      this._refresh_list();
    })
  }
  _refresh_list() {
    //this.databases = this.DatabaseService.query({dbname:'sandiegodb'});
    this.databases = this.DatabaseService.query();
  }
  setCurrent(database) {
    //console.log(JSON.stringify(database));
    this.scope.$emit('currentdb:change', database);
    this.currentDatabase = database;
  }
  fetch(database){
    this.DatabaseService.get({name:database.database_name});
  }
  // resetCurrentDB(){
  //   this.currentDB.name = undefined;
  // }
  //   remove(s, ev) {
  //     let confirm = this.mdDialog.confirm()
  //       .title(`Would you like to delete server ${s.server_name}?`)
  //       .targetEvent(ev)
  //       .ok('YES')
  //       .cancel('NO');

  //     this.mdDialog.show(confirm).then(() => {
  //       this.ServerService.remove({ name: s.server_name }, s,
  //         (resp, fn, code, text) => {
  //           // console.log(JSON.stringify(resp));
  //           // console.log(code);
  //           // console.log(text);
  //           this.mdDialog.cancel();
  //           this._refresh_list();
  //         },
  //         (err) => {
  //           console.log(JSON.stringify(err.data));
  //           console.log(JSON.stringify(err));
  //         })
  //     }, () => {
  //     });
  //   };

  //   fetch(d, ev) {
  //     this.mdDialog.show({
  //       template: html,
  //       targetEvent: ev,
  //       clickOutsideToClose: true,
  //       controller: ($scope, $mdDialog) => {
  //         $scope.data = d;
  //         $scope.cancel = () => {
  //           $mdDialog.cancel();
  //           this._refresh_list();
  //         };
  //         $scope.save = (data) => {
  //           this.ServerService.update({ name: data.server_name }, data,
  //             (resp, fn, code, text) => {
  //               // console.log(JSON.stringify(resp));
  //               // console.log(code);
  //               // console.log(text);
  //               $scope.cancel();
  //             },
  //             (err) => {
  //               $scope.error = err;
  //               // console.log(JSON.stringify(err.data));
  //               // console.log(JSON.stringify(err));
  //             });
  //         };
  //       }
  //     })

  //   }
  //   submit(s) {
  //     console.log('submit form');
  //     this.ServerService.update({ name: s.server_name }, s, function (resp, fn, code, text) {
  //       console.log(JSON.stringify(resp));
  //       console.log(code);
  //       console.log(text);
  //     }, function (err) {
  //       console.log(JSON.stringify(err.data));
  //       console.log(JSON.stringify(err));
  //     });
  //   }
}
DatabaseController.$inject = ['$scope', '$mdDialog', 'DatabaseService'];
export { DatabaseController };


class TableController {
  constructor(scope, element, attrs, s) {
    this.TableService = s;
    this.scope = scope;
    this.element = element;
    this.tables = s.table.query({ dbname: scope.currentdb.database_name })
    this.scope.$on('refresh:tablelist', (event, database) => {
      this.tables = s.table.query({ dbname: database.database_name })
    })
  }
  setCurrent(table) {
    //console.log('before emit table change')
    this.scope.$emit('currenttable:change', table);
    //this.currentTable = table;
    //this.alerts_and_deps(this.scope.currentdb.database_name, table);
  }
  fetch(table) {
    this.TableService.table.get({ dbname: this.scope.currentdb.database_name, name: table.table_name });
  }
  alerts_and_deps(db, table) {
    this.currentTable = table;
    let n = table.table_name;
    this.alerts = this.TableService.table_alerts.query({ dbname: db, name: n })
    this.deps = this.TableService.table_deps.get({ dbname: db, name: n })
  }
  //   remove(s, ev) {
  //     let confirm = this.mdDialog.confirm()
  //       .title(`Would you like to delete server ${s.server_name}?`)
  //       .targetEvent(ev)
  //       .ok('YES')
  //       .cancel('NO');

  //     this.mdDialog.show(confirm).then(() => {
  //       this.ServerService.remove({ name: s.server_name }, s,
  //         (resp, fn, code, text) => {
  //           // console.log(JSON.stringify(resp));
  //           // console.log(code);
  //           // console.log(text);
  //           this.mdDialog.cancel();
  //           this._refresh_list();
  //         },
  //         (err) => {
  //           console.log(JSON.stringify(err.data));
  //           console.log(JSON.stringify(err));
  //         })
  //     }, () => {
  //     });
  //   };

  //   fetch(d, ev) {
  //     this.mdDialog.show({
  //       template: html,
  //       targetEvent: ev,
  //       clickOutsideToClose: true,
  //       controller: ($scope, $mdDialog) => {
  //         $scope.data = d;
  //         $scope.cancel = () => {
  //           $mdDialog.cancel();
  //           this._refresh_list();
  //         };
  //         $scope.save = (data) => {
  //           this.ServerService.update({ name: data.server_name }, data,
  //             (resp, fn, code, text) => {
  //               // console.log(JSON.stringify(resp));
  //               // console.log(code);
  //               // console.log(text);
  //               $scope.cancel();
  //             },
  //             (err) => {
  //               $scope.error = err;
  //               // console.log(JSON.stringify(err.data));
  //               // console.log(JSON.stringify(err));
  //             });
  //         };
  //       }
  //     })

  //   }
  //   submit(s) {
  //     console.log('submit form');
  //     this.ServerService.update({ name: s.server_name }, s, function (resp, fn, code, text) {
  //       console.log(JSON.stringify(resp));
  //       console.log(code);
  //       console.log(text);
  //     }, function (err) {
  //       console.log(JSON.stringify(err.data));
  //       console.log(JSON.stringify(err));
  //     });
  //   }
}
TableController.$inject = ['$scope', '$element', '$attrs', 'TableService'];
export { TableController };

import html from './tableAlertsAndDeps.html';
import throbberhtml from '../common/throbber.html'
let update = (scope, s, mdDialog) => {
  mdDialog.show({
    template: throbberhtml,
    parent: angular.element(document.body),
    escapeToClose: false,
    clickOutsideToClose: false,
    fullscreen: false
  })
  scope.currenttable.alerts = s.table_alerts.query({ dbname: scope.currentdb.database_name, name: scope.currenttable.table_name });
  scope.currenttable.deps = s.table_deps.get({ dbname: scope.currentdb.database_name, name: scope.currenttable.table_name }, null,
    (resp, fn, code, text) => {
      mdDialog.cancel();
    });
}
let TableAlertsAndDepsDirective = () => {
  return {
    template: html,
    scope: {
      currentdb: '=',
      currenttable: '='
    },
    controller: ['$scope', 'TableService', '$mdDialog', function ($scope, TableService, mdDialog) {
      update($scope, TableService, mdDialog);
      $scope.$on('refresh:tablealertsanddeps', function (event, table) {
        $scope.currenttable = table;
        update($scope, TableService, mdDialog);
      })
    }]
  }
}

export default TableAlertsAndDepsDirective;

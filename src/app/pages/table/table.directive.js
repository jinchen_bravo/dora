import html from './table-list.html';
import { TableController as tblctrl } from './table.controller';
let TableDirective = () => {
  return {
    template: html,
    controller: tblctrl,
    controllerAs: 'tblctrl',
    scope: {
      currentdb: "="
    }
  }
}

export default TableDirective;

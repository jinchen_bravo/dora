import TableDirective from './table.directive';
import AlertsAndDepsDirective from './alertsanddeps.directive';
import '../../services';
export default angular
  .module('app.pages.tables', ['zApp.service'])
  .directive('tableList', TableDirective)
  .directive('tableAlertsDeps', AlertsAndDepsDirective);

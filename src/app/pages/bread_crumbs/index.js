import html from './bread_crumbs.html';
//import breadcrumbsDirective from './bread_crumbs';
import './bread_crumbs.css';
export default angular
  .module('app.pages.breadcrumbs', ['zApp.service'])
  //.directive('breadCrumbs', breadcrumbsDirective);
  .directive('breadCrumbs', function () {
    return {
      controller: function ($scope, $element, $attrs) {
        $scope.action = function (breadcrumb) {
          //console.log('before database:reset emit');
          $scope.$emit(breadcrumb.action, breadcrumb.label);
        }
        // $attrs.$observe('breadcrumbs', (value) => {
        //   //console.log(value);
        //   if (value) {
        //     $scope.breadcrumbs = JSON.parse(value);
        //     //console.log($scope);
        //   }
        // });
      },
      // link: function ($scope, $element, $attrs) {
      //   $scope.$watch($attrs.breadcrumbs, function (n, o, scope) {
      //     //console.log(JSON.stringify(n));
      //     // console.log(JSON.stringify(o));
      //     scope.breadcrumbs = n;
      //   })
      // },
      template: html,
      scope: {
        breadcrumbs: "="
      }
    }
  });

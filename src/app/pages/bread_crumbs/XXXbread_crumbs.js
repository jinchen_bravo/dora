import html from './bread_crumbs.html';
export default {
    template: html,
    scope: {
        breadcrumbs: "@"
    },
    controller: ['$scope', '$attrs', function ($scope, $attrs) {
        // $scope.breadcrumbs = [{ label: 'Databases' }, { label: 'db1' }, { label: 'tb1' }]
        $attrs.$observe('breadcrumbs', (value) => {
            if (value) {
                $scope.breadcrumbs = value;
            }
        });
    }]
    //replace: true
}

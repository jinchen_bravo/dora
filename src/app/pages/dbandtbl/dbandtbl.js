import html from './dbandtbl.html';
export default {
    template: html,
    conntrollerAs: 'dbandtblctrl',
    controller: ['$scope', function ($scope) {
        $scope.breadcrumbs = [{ label: 'Databases', action: 'databases:reset' }];
        $scope.reset = function () {
            $scope.currentTable = undefined;
            $scope.currentDatabase = undefined;
            $scope.breadcrumbs = [];
            $scope.breadcrumbs.push({ label: 'Databases', action: 'databases:reset' });
        }
        $scope.refresh = function () {
            // console.log('before refresh db list');
            // console.log($scope.currentDatabase);
            $scope.$broadcast('refresh:databaselist');
            if ($scope.currentDatabase) {
                $scope.$broadcast('refresh:tablelist', $scope.currentDatabase);
            }
        }
        $scope.$on('databases:reset', function () {
            //console.log('database:reset');
            $scope.reset();
        })
        $scope.$on('currentdb:change', function (event, database) {
            //console.log(JSON.stringify(database));
            let d = undefined;
            if (database.database_name === undefined) {
                d = { database_name: database }
            } else {
                d = database;
            }
            $scope.reset();
            $scope.currentDatabase = d;
            $scope.breadcrumbs.push({ label: d.database_name, action: 'currentdb:change' });
        })
        $scope.$on('currenttable:change', function (event, table) {
            let t = undefined;
            if (table.table_name === undefined) {
                t = { table_name: table };
            } else {
                t = table;
            }
            $scope.currentTable = t;
            if ($scope.breadcrumbs.length === 3) {
                $scope.breadcrumbs.pop();
            }
            $scope.breadcrumbs.push({ label: t.table_name, action: 'currenttable:change' });
            $scope.$broadcast('refresh:tablealertsanddeps', t);
        })
    }]
}

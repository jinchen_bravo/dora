import DbandtblDirective from './dbandtbl';
import '../../services';
import '../database';
import '../table';
import '../bread_crumbs';
export default angular
  .module('app.pages.dbandtbl', ['zApp.service', 'app.pages.databases', 'app.pages.tables', 'app.pages.breadcrumbs'])
  .component('dbandtbl', DbandtblDirective);

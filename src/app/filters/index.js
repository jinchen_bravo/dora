export default angular
    .module('app.filters', [])
    .filter('trimLength', () => {
        return (input, length) => {
            if (input.length > length) {
                return `${input.substring(0, length - 3)}...`;
            } else {
                return input;
            }
        }
    });

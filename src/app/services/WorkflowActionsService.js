let WorkflowActionsService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return $resource(`${RESTURL}/workflows/:name/action`, null,
        {
            delete: {
                method: 'DELETE',
                headers: {
                    Authorization: basicReadWrite
                }
            },
            action: {
                method: 'PUT',
                headers: {
                    Authorization: basicReadWrite
                }
            },
        }
    )
}

let WorkflowExecutionsService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return $resource(`${RESTURL}/workflows/:name/execution`, null,
        {
            execution: {
                method: 'PUT',
                headers: {
                    Authorization: basicReadWrite
                }
            },
        }
    )
}

export { WorkflowActionsService, WorkflowExecutionsService }
import commonService from './commonService'
export default ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return commonService($resource, RESTURL, basicReadOnly, basicReadWrite,'databases/:name');
}

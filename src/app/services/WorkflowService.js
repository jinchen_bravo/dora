import commonService from './commonService'
import { WorkflowActionsService, WorkflowExecutionsService } from './WorkflowActionsService'
let wfService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return commonService($resource, RESTURL, basicReadOnly, basicReadWrite, 'workflows/:name');
}
let SSWorkflowService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return commonService($resource, RESTURL, basicReadOnly, basicReadWrite, 'workflows/systemseedings/:name');
}
let OBJWorkflowService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return commonService($resource, RESTURL, basicReadOnly, basicReadWrite, 'workflows/objectseedings/:name');
}

let WorkflowService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return {
        workflow: wfService($resource, RESTURL, basicReadOnly, basicReadWrite),
        system_seeding: SSWorkflowService($resource, RESTURL, basicReadOnly, basicReadWrite),
        object_seeding: OBJWorkflowService($resource, RESTURL, basicReadOnly, basicReadWrite),
        workflow_action: WorkflowActionsService($resource, RESTURL, basicReadOnly, basicReadWrite),
        workflow_execution: WorkflowExecutionsService($resource, RESTURL, basicReadOnly, basicReadWrite)
    }
}
export { WorkflowService }

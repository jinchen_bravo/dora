export default ($resource, RESTURL, basicReadOnly, basicReadWrite, subURL) => {
    return $resource(`${RESTURL}/${subURL}`, null,
        {
            get: {
                method: 'GET',
                headers: {
                    Authorization: basicReadOnly
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    Authorization: basicReadWrite
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    Authorization: basicReadWrite
                }
            },
            query: {
                method: 'GET',
                isArray: true,
                headers: {
                    Authorization: basicReadOnly
                }
            }
        }
    )
}
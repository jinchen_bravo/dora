import ServerService from './ServerService';
import { WorkflowService } from './WorkflowService';
import DatabaseService from './DatabaseService';
import { TableService } from './TableService';
import 'angular-resource';
ServerService.$inject = ['$resource', 'baseurl', 'basicReadOnly', 'basicReadWrite']
WorkflowService.$inject = ['$resource', 'baseurl', 'basicReadOnly', 'basicReadWrite']
DatabaseService.$inject = ['$resource', 'baseurl', 'basicReadOnly', 'basicReadWrite']
TableService.$inject = ['$resource', 'baseurl', 'basicReadOnly', 'basicReadWrite']
export default angular
  .module('zApp.service', ['ngResource'])
  .factory('ServerService', ServerService)
  .factory('WorkflowService', WorkflowService)
  .factory('DatabaseService', DatabaseService)
  .factory('TableService', TableService)
  // .config(['$resourceProvider', function ($resourceProvider) {
  //   // Don't strip trailing slashes from calculated URLs
  //   $resourceProvider.defaults.stripTrailingSlashes = false;
  // }])
import commonService from './commonService'
let TService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return commonService($resource, RESTURL, basicReadOnly, basicReadWrite, 'databases/:dbname/tables/:name');
}
let AlertsService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return commonService($resource, RESTURL, basicReadOnly, basicReadWrite, 'databases/:dbname/tables/:name/alerts');
}
let DepsService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return commonService($resource, RESTURL, basicReadOnly, basicReadWrite, 'databases/:dbname/tables/:name/dependencies');
}
let TableService = ($resource, RESTURL, basicReadOnly, basicReadWrite) => {
    return {
        table: TService($resource, RESTURL, basicReadOnly, basicReadWrite),
        table_alerts: AlertsService($resource, RESTURL, basicReadOnly, basicReadWrite),
        table_deps: DepsService($resource, RESTURL, basicReadOnly, basicReadWrite)
    }
}
export { TableService }

import { workflowFactory } from './workflow'

export default angular
    .module('app.models', [])
    .factory('workflowFactory', workflowFactory);

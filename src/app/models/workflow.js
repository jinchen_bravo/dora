let workflowFactory = () => {
    return {
        system_seeding: {
            create(...args) {
                return new SystemseedingWorkflow(...args);
            }
        },
        object_seeding: {
            create(...args) {
                return new ObjectseedingWorkflow(...args);
            }
        }
    };
}
class Workflow {
    constructor(name) {
        this.name = name;
    }
}
class SeedingWorkflow extends Workflow {
    constructor(name) {
        super(name);
        this.rpo_interval = 1;
    }
    set srcdb(h) {
        this.source_td_pid = h.name;
        this.source_db_user_name = h.credential.username;
        this.source_db_user_password = h.credential.password;
    }
    set tgtdb(h) {
        this.target_td_pid = h.name;
        this.target_db_user_name = h.credential.username;
        this.target_db_user_password = h.credential.password;
    }
    set cam(v) {
        this.cam_action_name = v;
    }
}

class SystemseedingWorkflow extends SeedingWorkflow {
    constructor(name) {
        super(name);
    }
    set tgtos(h) {
        this.target_os_host_name = h.name;
        this.target_os_user_name = h.credential.username;
        this.target_os_user_password = h.credential.password;
    }
    // set tgtoskey(h) {
    //     this.target_os_host_name = h.name;
    //     this.target_os_user_name = h.credential.username;
    //     this.target_os_key = h.credential.password;
    // }
    set tgtauthtype(t) {
        this.target_authentication_type = t;
    }
    set st(stype) {
        this.seeding_type = stype;
    }
    set sm(sm) {
        this.seeding_mechanism = sm;
    }

    set encrypt(v) {
        this.encryption_flag = v;
    }
}

class ObjectseedingWorkflow extends SeedingWorkflow {
    constructor(name) {
        super(name);
    }
    set ua(ua) {
        this.user_action = ua;
    }
    set dm(d) {
        this.dm_job_server_name = d.server_name;
        this.dm_job_user_name = d.credential.username;
        this.dm_job_user_password = d.credential.password;
        this.dm_job_permission = d.permission;
    }
    set objs(o) {
        this.objectSelectionList = o;
    }
}

export { workflowFactory, SystemseedingWorkflow, ObjectseedingWorkflow }
